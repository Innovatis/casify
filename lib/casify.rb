require File.join(File.dirname(__FILE__), 'casify', 'rubycas-client-rails')
require 'active_support/concern'
require 'rest_client'
require 'json'

module Casify
  extend ActiveSupport::Concern

  included do
    before_filter :assign_current_user_to_thread_current
  end

  module InstanceMethods
    def current_user_apps
      JSON.parse(RestClient.get("#{Rails.application.config.rubycas.cas_base_url}authInfo?username=#{current_user.email}"))['apps']
    end

    def current_user
      return nil unless session[:cas_user].present?
      user = User.where(:email => session[:cas_user]).first
      unless user
        user = User.new
        user.email = session[:cas_user]
        user.save!
      end
      user
    end

    def user_signed_in?
      !! current_user
    end

    def assign_current_user_to_thread_current
      Thread.current[:current_user] = current_user
    end
  end
end


module ActionView::Helpers

  def current_user
    controller.current_user
  end

  def user_signed_in?
    !! current_user
  end

  def current_user_apps
    controller.current_user_apps
  end

  def network_menu(opts={})
    content_tag(:ul,:id => "network-menu", :class => opts[:ul_class]) do
      # not sure why safe_concat wasn't working
      all = ""
      current_user_apps.each do |app|
        a_class = "#{opts[:a_class]} #{(app['service_url'].match(/#{request.host}/) ? :current : '')}"
        all << content_tag(:li, :class => "network-menu-item #{opts[:li_class]}", :id => "#{app['name']}-network-menu-item") do
          link_to app['name'], "//#{app['service_url']}", :class => a_class
        end
      end
      all << content_tag(:li, :class => "#{opts[:li_class]} logout network-menu-item") do
        link_to "Log out (#{current_user.try(:email) || current_user.try(:username)})", '/logout', :class => opts[:a_class]
      end
      all.html_safe
    end
  end

end


