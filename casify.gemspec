# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "casify/version"

Gem::Specification.new do |s|
  s.name        = "casify"
  s.version     = Casify::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Burke Libbey"]
  s.email       = ["burke@burkelibbey.org"]
  s.homepage    = ""
  s.summary     = %q{Innovatis CAS plugin}
  s.description = %q{Innovatis CAS plugin}

  s.add_dependency "rubycas-client"
  s.add_dependency "rest-client"
  s.add_dependency "json"
  
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
